﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ToyRobot.Tests
{
    [TestClass]
    public  class ConstraintsTests
    {

        /// <summary>
        /// The toy robot must not fall off the table during movement. 
        /// This also includes the initial placement of the toy robot
        /// 
        /// - Any move that would cause the robot to fall must be ignored
        /// - Example Input and Output:
        /// 
        /// o PLACE 0,0,NORTH
        /// o MOVE
        /// o REPORT
        /// Output: 0,1,NORTH
        /// 
        /// o PLACE 0,0,NORTH
        /// o LEFT
        /// o REPORT
        /// Output: 0,0,WEST
        /// 
        /// o PLACE 1,2,EAST
        /// o MOVE
        /// o MOVE
        /// o LEFT
        /// o MOVE
        /// o REPORT
        ///  Output: 3,3,NORTH
        /// 
        /// Maintains internal state for position and facing direction.
        /// 
        /// </summary>

        [TestMethod]
        public void When_Robot_PlacedOutoOffTable_MustBeIgnored()
        {
            var robot = new Robot();
            var result = robot.Place(-1, 0, Facing.North);
            Assert.IsFalse(result);
            Assert.AreEqual("Robot cannot be placed there.", robot.LastError);

            result = robot.Place(0, 6, Facing.North);
            Assert.IsFalse(result);
            Assert.AreEqual("Robot cannot be placed there.", robot.LastError);
        }

        [TestMethod]
        public void When_Robot_PlacedAndMovedOffTable_CannotBeMoved()
        {
            var robot = new Robot();
            robot.Place(4, 4, Facing.North);
            var result = robot.Move();
            Assert.IsFalse(result);
            Assert.AreEqual("Robot cannot be moved there.", robot.LastError);
        }


        [TestMethod]
        public void RobotOperator_PlacedAndMoved_ReportsCorrectPosition()
        {
            var robot = new Robot();
            robot.Place(0, 0, Facing.North);
            robot.Move();
            var result = robot.Report();
            Assert.AreEqual("0,1,NORTH", result);
        }


        [TestMethod]
        public void RobotOperator_PlacedMovedAndTurned_ReportsCorrectPosition()
        {
            var robot = new Robot();
            robot.Place(1, 2, Facing.East);
            robot.Move();
            robot.Move();
            robot.Left();
            robot.Move();
            Assert.AreEqual("3,3,NORTH", robot.Report());
        }
    }
}
