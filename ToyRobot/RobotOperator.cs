﻿using System;

namespace ToyRobot
{
    /// <summary>
    /// RobotOperator class.  Responsible for receiving and parsing commands for the robot and passing them on.
    /// </summary>
    public class RobotOperator
    {
        public RobotOperator(Robot robot)
        {
            Robot = robot;
        }

        public Robot Robot { get; set; }

        public string Command(string command)
        {
            string response;
            CommandArguments args = null;
            var instruction = GetInstruction(command, ref args);

            switch (instruction)
            {
                case Instruction.Place:
                    var placeArgs = (PlaceInstructionArguments)args;
                    response = Robot.Place(placeArgs.X, placeArgs.Y, placeArgs.Facing) ? "Done." : Robot.LastError;
                    break;
                case Instruction.Move:
                    response = Robot.Move() ? "Done." : Robot.LastError;
                    break;
                case Instruction.Left:
                    response = Robot.Left() ? "Done." : Robot.LastError;
                    break;
                case Instruction.Right:
                    response = Robot.Right() ? "Done." : Robot.LastError;
                    break;
                case Instruction.Report:
                    response = Robot.Report();
                    break;
                default:
                    response = "Invalid command.";
                    break;
            }
            return response;
        }

        private static Instruction GetInstruction(string command, ref CommandArguments args)
        {
            var argString = "";

            var argsSeperatorPosition = command.IndexOf(" ", StringComparison.Ordinal);
            if (argsSeperatorPosition > 0)
            {
                argString = command.Substring(argsSeperatorPosition + 1);
                command = command.Substring(0, argsSeperatorPosition);
            }
            command = command.ToUpper();

            if (Enum.TryParse<Instruction>(command, true, out var result))
            {
                if (result == Instruction.Place)
                {
                    if (!TryParsePlaceArgs(argString, ref args))
                    {
                        result = Instruction.Invalid;
                    }
                }
            }
            else
            {
                result = Instruction.Invalid;
            }
            return result;
        }

        private static bool TryParsePlaceArgs(string argString, ref CommandArguments args)
        {
            var argParts = argString.Split(',');

            if (argParts.Length != 3 || !TryGetCoordinate(argParts[0], out var x) ||
                !TryGetCoordinate(argParts[1], out var y) ||
                !TryGetFacingDirection(argParts[2], out var facing)) return false;
            args = new PlaceInstructionArguments 
            {
                X = x,
                Y = y,
                Facing = facing,
            };
            return true;
        }

        private static bool TryGetCoordinate(string coordinate, out int coordinateValue)
        {
            return int.TryParse(coordinate, out coordinateValue);
        }

        private static bool TryGetFacingDirection(string direction, out Facing facing)
        {
            return Enum.TryParse(direction, true, out facing);
        }
    }
}
