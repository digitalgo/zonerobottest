﻿namespace ToyRobot
{
    /// <summary>
    /// ToyRobot class.  
    /// Receives instructions from the operator, validates them and carries them out.
    /// Maintains internal state for position and facing direction.
    /// </summary>
    public class Robot
    {
        public Robot()
        {
            LastError = "";
        }

        private const int TableSize = 5;
        private int? _x;
        private int? _y;
        private Facing _facing;

        public string LastError { get; set; }

        public bool Place(int x, int y, Facing facing)
        {
            if (MandateIsOnTable(x, y, "placed"))
            {
                _x = x;
                _y = y;
                _facing = facing;
                return true;
            }
            return false;
        }

        public bool Move()
        {
            if (MandateIsPlaced("move"))
            {
                int newx = GetXAfterMove();
                int newy = GetYAfterMove();
                if (MandateIsOnTable(newx, newy, "moved"))
                {
                    _x = newx;
                    _y = newy;
                    return true;
                }
            }
            return false;
        }

        private int GetXAfterMove()
        {
            switch (_facing)
            {
                case Facing.East:
                    return _x.Value + 1;
                case Facing.West:
                    return _x.Value - 1;
            }
            return _x.Value;
        }

        private int GetYAfterMove()
        {
            switch (_facing)
            {
                case Facing.North:
                    return _y.Value + 1;
                case Facing.South:
                    return _y.Value - 1;
            }
            return _y.Value;
        }

        public bool Left()
        {
            return Turn(Direction.Left);
        }

        public bool Right()
        {
            return Turn(Direction.Right);
        }

        private bool Turn(Direction direction)
        {
            if (MandateIsPlaced("turn"))
            {
                var facingAsNumber = (int)_facing;
                facingAsNumber += 1 * (direction == Direction.Right ? 1 : -1);
                if (facingAsNumber == 5) facingAsNumber = 1;
                if (facingAsNumber == 0) facingAsNumber = 4;
                _facing = (Facing)facingAsNumber;
                return true;
            }
            return false;
        }

        public string Report()
        {
            if (MandateIsPlaced("report it's position"))
            {
                return $"{_x.Value},{_y.Value},{_facing.ToString().ToUpper()}";
            }
            return "";
        }

        private bool MandateIsPlaced(string action)
        {
            if (!_x.HasValue || !_y.HasValue)
            {
                LastError = $"Robot cannot {action} until it has been placed on the table.";
                return false;
            }
            return true;
        }

        private bool MandateIsOnTable(int x, int y, string action)
        {
            if (x < 0 || y < 0 || x >= TableSize || y >= TableSize)
            {
                LastError = $"Robot cannot be {action} there.";
                return false;
            }
            return true;
        }
    }
}