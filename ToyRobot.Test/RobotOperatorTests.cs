﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ToyRobot.Tests
{
    [TestClass]
    public class RobotOperatorTests
    {
        [TestMethod]
        public void RobotOperator_InitialisedRobotOperator_ControlsRobot()
        {
            var robotOperator = new RobotOperator(new Robot());
            Assert.IsNotNull(robotOperator.Robot);
        }

        [TestMethod]
        public void Robot_InitialisedButNotPlaced_CannotBeMoved()
        {
            var robot = new Robot();
            var result = robot.Move();
            Assert.IsFalse(result);
            Assert.AreEqual("Robot cannot move until it has been placed on the table.", robot.LastError);
        }

        [TestMethod]
        public void Robot_InitialisedButNotPlaced_CannotBeTurned()
        {
            var robot = new Robot();
            var result = robot.Left();
            Assert.IsFalse(result);
            Assert.AreEqual("Robot cannot turn until it has been placed on the table.", robot.LastError);
        }

        [TestMethod]
        public void RobotOperator_EmptyCommand_ReportsInvalid()
        {
            var robotOperator = new RobotOperator(new Robot());
            var response = robotOperator.Command("");
            Assert.AreEqual("Invalid command.", response);
        }

        [TestMethod]
        public void RobotOperator_UnrecognisedCommand_ReportsInvalid()
        {
            var robotOperator = new RobotOperator(new Robot());
            var response = robotOperator.Command("XXXX");
            Assert.AreEqual("Invalid command.", response);
        }

        [TestMethod]
        public void RobotOperator_RecognisedCommand_ReportsValid()
        {
            var robotOperator = new RobotOperator(new Robot());
            var response = robotOperator.Command("MOVE");
            Assert.AreEqual("Robot cannot move until it has been placed on the table.", response);
        }

        [TestMethod]
        public void RobotOperator_PlaceCommandWithNoArguments_ReportsInvalid()
        {
            var robotOperator = new RobotOperator(new Robot());
            var response = robotOperator.Command("PLACE");
            Assert.AreEqual("Invalid command.", response);
        }

        [TestMethod]
        public void RobotOperator_PlaceCommandWithInvalidArguments_ReportsInvalid()
        {
            var driver = new RobotOperator(new Robot());
            var response = driver.Command("PLACE XXX");
            Assert.AreEqual("Invalid command.", response);
            response = driver.Command("PLACE 1,X,NORTH");
            Assert.AreEqual("Invalid command.", response);
            response = driver.Command("PLACE X,1,NORTH");
            Assert.AreEqual("Invalid command.", response);
            response = driver.Command("PLACE 1,1,XXX");
            Assert.AreEqual("Invalid command.", response);
        }

    }
}
