﻿using System;

namespace ToyRobot
{
    class Program
    {
        static void Main(string[] args)
        {
            DisplayWelcome();

            var driver = new RobotOperator(new Robot());

            while (true)
            {
                string command = PromptForCommand();
                if (command.ToUpper() == "EXIT" || command.ToUpper() == "QUIT")
                {
                    Environment.Exit(0);
                }
                Console.WriteLine(driver.Command(command));
            }
        }

        private static void DisplayWelcome()
        {
            Console.WriteLine("Zone- Toy Robot Test");
            Console.WriteLine("=====================");
            Console.WriteLine("");
        }

        private static string PromptForCommand()
        {
            Console.WriteLine("\n>>: ");
            return Console.ReadLine();
        }


    }
}
