﻿namespace ToyRobot
{
    public class PlaceInstructionArguments : CommandArguments
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Facing Facing { get; set; }
    }
}
