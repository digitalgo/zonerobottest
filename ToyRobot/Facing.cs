﻿namespace ToyRobot
{
    public enum Facing : byte
    {
        North = 1,
        East = 2,
        South = 3,
        West = 4,
    }
}
